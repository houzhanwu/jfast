/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.job;

import java.util.ArrayList;
import java.util.List;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;
import com.jfast.framework.log.Logger;

public class JobManger {
	
	private static final Logger logger = Logger.getLogger(JobManger.class);
	
	public static List<JfastJobBuild> jobs = new ArrayList<JfastJobBuild>();
	
	private static final JobManger jobManger = new JobManger();
	
	private JobManger(){}
	
	public static JobManger getJobManger() {
		return jobManger;
	}

	public void addJfastBuild(JfastJobBuild jfastBuild) {
		jobs.add(jfastBuild);
	}
	
	public void removeJfastBuild(JfastJobBuild jfastBuild) {
		jobs.remove(jfastBuild);
	}

	public static List<JfastJobBuild> getJobs() {
		return jobs;
	}
	
	public void start(){
		try {
			Scheduler scheduler = new StdSchedulerFactory().getScheduler();
			for (JfastJobBuild job : jobs) {
				job.build(scheduler);
			}
			scheduler.start();
			logger.info("任务启动成功");
		} catch (SchedulerException e) {
			logger.error("job start error.........",e);
			System.exit(-1);
		}
	}
}
