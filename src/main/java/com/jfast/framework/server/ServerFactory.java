/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.server;

import com.jfast.framework.bean.ClassFactory;
import com.jfast.framework.server.jetty.JettyServer;
import com.jfast.framework.server.tomcat.TomcatServer;
import com.jfast.framework.server.undertow.UndertowServer;
import com.jfast.framework.web.config.JfastConfigManager;

/**
 * server实例工厂
 * @author zengjintao
 * @version 1.0
 * @createTime 2017年12月27日上午10:19:17
 */
public class ServerFactory {
	
	private ClassFactory classFactory;
	
    private ServerFactory(){
		
	}
    
    public void setClassFactory(ClassFactory classFactory) {
		this.classFactory = classFactory;
	}
    
    public ClassFactory getClassFactory() {
		return classFactory;
	}
	
	private static final ServerFactory serverFactory = new ServerFactory();
	
	public static ServerFactory getServerFactory() {
		return serverFactory;
	}
	
	public JfastServer createServer(Class<? extends JfastServer> clazz){
		return classFactory.createSingletonBean(clazz);
	}
	
	public JfastServer createServer(){
		JfastServerConfig jfastServerConfig = JfastConfigManager.createConfigBean(JfastServerConfig.class);
		switch(jfastServerConfig.getType()){
		   case JfastServerConfig.TYPE_TOMCAT :
			   return new TomcatServer();
		   case JfastServerConfig.TYPE_JETTY :
			   return new JettyServer();
		   case JfastServerConfig.UNDERTOW :
			   return new UndertowServer();
		   default :
			   return new TomcatServer();
		}
	}
}
