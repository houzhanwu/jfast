/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.server;

import com.jfast.framework.web.annotation.ConfigurationProperties;


/**
 * jfast服务器配置类
 * @author zengjintao
 * @version 1.0
 * @createTime 2018年1月2日下午7:28:00
 */
@ConfigurationProperties(prefix = "jfast.server")
public class JfastServerConfig {

    public static final String TYPE_TOMCAT = "tomcat";
    public static final String TYPE_JETTY = "jetty";
    public static final String UNDERTOW = "undertow";
    
    private String host = "127.0.0.1";
	private int port = 8080;
	private String contextPath = "/";
	private String type = TYPE_JETTY;
	private String resourceLocation = "src/main/webapp";
	
	private static final long DEFAULT_SCANSECONDS= 5L;
	private long scanSeconds = DEFAULT_SCANSECONDS;
	
	public long getScanSeconds() {
		return scanSeconds;
	}

	public void setScanSeconds(long scanSeconds) {
		this.scanSeconds = scanSeconds;
	}

	public String getResourceLocation() {
		return resourceLocation;
	}

	public void setResourceLocation(String resourceLocation) {
		this.resourceLocation = resourceLocation;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getPort() {
		return port;
	}
	
	public void setPort(int port) {
		this.port = port;
	}
	
	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getContextPath() {
		return contextPath;
	}

	public void setContextPath(String contextPath) {
		this.contextPath = contextPath;
	}
	
	@Override
	public String toString() {
		return "JfastServerConfig {" +
                "type='" + type + '\'' +
                ", host='" + host + '\'' +
                ", port=" + port +
                ", contextPath='" + contextPath + '\'' +
                '}';
	}
}
