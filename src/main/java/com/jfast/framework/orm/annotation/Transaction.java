package com.jfast.framework.orm.annotation;

import com.jfast.framework.orm.config.DataSourceManager;

import java.lang.annotation.*;
import java.sql.Connection;

/**
 * 数据库事物注解
 * @author zengjintao
 * @version 1.0
 * @createTime 2018年1月15日下午8:02:30
 */
@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Transaction {
	
	String configName() default DataSourceManager.DEFAULT_DATASOURCE_NAME;
	
	int transactionIsolation() default Connection.TRANSACTION_REPEATABLE_READ;;
}
