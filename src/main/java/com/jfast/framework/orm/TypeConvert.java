/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.orm;

import com.jfast.framework.web.core.JfastConstant;

import java.util.HashMap;
import java.util.Map;

public final class TypeConvert {

	private static final Map<Object, String> classMap = new HashMap<Object, String>();

	static {
		classMap.put(String.class, "String");
		classMap.put(Integer.class, "Integer");
		classMap.put(int.class, "int");
		classMap.put(double.class, "double");
		classMap.put(Float.class, "Float");
		classMap.put(float.class, "float");
		classMap.put(Long.class, "Long");
		classMap.put(java.math.BigInteger.class, "BigInteger");
		classMap.put(java.math.BigDecimal.class, "BigDecimal");
	}
	
	
	/**
     * 数据转化
     * @param type
     * @param s
     * @return
     */
	public static final Object convert(Class<?> type, String s) {

        if (type == String.class) {
            return s;
        }
        // mysql type: int, integer, tinyint(n) n > 1, smallint, mediumint
        if (type == Integer.class || type == int.class) {
            return Integer.parseInt(s);
        }

        // mysql type: bigint
        if (type == Long.class || type == long.class) {
            return Long.parseLong(s);
        }

        // mysql type: real, double
        if (type == Double.class || type == double.class) {
            return Double.parseDouble(s);
        }

        // mysql type: float
        if (type == Float.class || type == float.class) {
            return Float.parseFloat(s);
        }

        // mysql type: bit, tinyint(1)
        if (type == Boolean.class || type == boolean.class) {
            String value = s.toLowerCase();
            if (JfastConstant.TRUE.equals(value) || "true".equals(value)) {
                return Boolean.TRUE;
            } else if (JfastConstant.FALSE.equals(value) || "false".equals(value)) {
                return Boolean.FALSE;
            } else {
                throw new RuntimeException("Can not parse to boolean type of value: " + s);
            }
        }

        // mysql type: decimal, numeric
        if (type == java.math.BigDecimal.class) {
            return new java.math.BigDecimal(s);
        }

        // mysql type: unsigned bigint
        if (type == java.math.BigInteger.class) {
            return new java.math.BigInteger(s);
        }

        // mysql type: binary, varbinary, tinyblob, blob, mediumblob, longblob. I have not finished the test.
        if (type == byte[].class) {
            return s.getBytes();
        }
        throw new RuntimeException(type.getName() + " can not be converted, please use other type in your config class!");
    }
	
	public static boolean isNumberOrString(Class<?> cla){
		return classMap.get(cla) != null ? true : false;
	}
	
	public static String getClassTypeToString(Class<?> cla){
		return classMap.get(cla);
	}
}
