/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.orm.config;

import com.jfast.framework.kit.StrKit;
import com.jfast.framework.web.config.JfastConfigManager;

import java.util.*;
import java.util.Map.Entry;

/**
 * 数据库配置管理器
 * @author zengjintao
 * @version 1.0
 * @createTime 2017年11月12日下午8:38:23
 */
public class DataSourceConfigManager {

    private static final String DATASOURCE_PREFIX = "jfast.dataSource.";
	
	private static final DataSourceConfigManager dataSourceConfigManager = new DataSourceConfigManager();
	
	private List<DataSourceConfig> dataSourceConfigs = new ArrayList<>();
	
	public static DataSourceConfigManager getDataSourceConfigManager() {
		return dataSourceConfigManager;
	}
	
	public List<DataSourceConfig> getDataSourceConfigs() {
		return dataSourceConfigs;
	}
	    
	private DataSourceConfigManager() {
		
    	DataSourceConfig dataSourceConfig = JfastConfigManager.createConfigBean(DataSourceConfig.class);
    	
    	if (dataSourceConfig.isOk()) {
    		dataSourceConfigs.add(dataSourceConfig);
    	}
    	
		Map<String, Object> properties = JfastConfigManager.DEFAULT_PROPKIT.getKeyAndValue();
		
		Set<String> names = new HashSet<>();
		
		for (Entry<String, Object> e: properties.entrySet()) {
			if (e.getKey().startsWith(DATASOURCE_PREFIX)) {
				String spiltKeys[] = StrKit.spilt(e.getKey(), "\\.");
				if (spiltKeys.length == 4) {
					names.add(spiltKeys[2]);
				}
			}
		}
		
		for (String name : names) {
			DataSourceConfig dataSource = JfastConfigManager.createConfigBean(DataSourceConfig.class,DATASOURCE_PREFIX + name);
			dataSource.setName(name);
			dataSourceConfigs.add(dataSource);
		}
	}
}
