/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.orm.config;

import com.jfast.framework.bean.ClassFactory;
import com.jfast.framework.bean.JdkClassFactory;
import com.jfast.framework.orm.dataSource.DataSourceFactory;
import com.jfast.framework.orm.dialect.Dialect;
import com.jfast.framework.orm.dialect.MysqlDialect;
import com.jfast.framework.orm.dialect.OracleDialect;
import com.jfast.framework.orm.dialect.SqlServerDialect;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class DataSourceManager {
	
	public static final String DEFAULT_DATASOURCE_NAME = "main";
	private static Map<String, DbConfig> dataSourceMap = new HashMap<>();
	private static Map<String, TableMapping> tableMappingMap = new HashMap<>();
	private static List<DbConfig> dbConfigs = new ArrayList<>();
	private static ClassFactory classFactory = new JdkClassFactory();
	
	
	public static void addDbConfig(DbConfig dbConfig){
		dataSourceMap.put(dbConfig.getDataSourceFactory().getConfigName(), dbConfig);
	}
	
	public static DbConfig getDbConfig(String dataSourceName){
		return dataSourceMap.get(dataSourceName);
	}
	
	public static DbConfig getDefaultDbConfig(){
		return dataSourceMap.get(DEFAULT_DATASOURCE_NAME);
	}
	
	public static void removeDbConfig(String dataSourceName){
		 dataSourceMap.remove(dataSourceName);
	}
	
    /**
     * 获取所有配置的数据源
     * @return
     */
    public static List<DbConfig> getAllDbConfigs() {
         for (String key : dataSourceMap.keySet()) {
        	 dbConfigs.add(dataSourceMap.get(key));
         }
         return dbConfigs;
    }
    
    public static void addTableMapping(String configName, TableMapping tableMapping) {
    	tableMappingMap.put(configName, tableMapping);
    }
    
    public static Map<String, TableMapping> getTableMappingMap() {
		return tableMappingMap;
	}
    
    public static TableMapping getTableMappingMap(String configName) {
		return tableMappingMap.get(configName);
	}

	/**
     * 获取Dialect
     * @param dbConfig
     * @return
     */
    public static Dialect getDialect(DbConfig dbConfig) {
    	
    	DataSourceConfig dataSourceConfig = getThisDataSourceConfig(dbConfig);
    	switch (dataSourceConfig.getType()) {
			case DataSourceConfig.TYPE_MYSQL:
				return new MysqlDialect();
			case DataSourceConfig.TYPE_SQLSERVER:
				return new SqlServerDialect();
			case DataSourceConfig.TYPE_ORACLE:
				return new OracleDialect();
			default:
				return new MysqlDialect();
		}
    }

	private static DataSourceConfig getThisDataSourceConfig(DbConfig dbConfig) {
		DataSourceConfigManager manager = DataSourceConfigManager.getDataSourceConfigManager();
		for (DataSourceConfig config : manager.getDataSourceConfigs()) {
			if (dbConfig.getDataSourceFactory().getConfigName().equals(config.getName())) {
				return config;
			}
		}
		return null;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static DataSourceFactory createDataSourceFactory(DataSourceConfig dataSourceConfig){
		String className = dataSourceConfig.getDataSourceClassName();
		try {
			Class<?> clazz = Class.forName(className);
			List params = new ArrayList();
			params.add(dataSourceConfig);
			DataSourceFactory dataSourceFactory = (DataSourceFactory) classFactory.createBean(clazz, params);
			return dataSourceFactory;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
}
