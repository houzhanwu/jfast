/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.orm.config;


import com.alibaba.druid.pool.DruidDataSource;
import com.jfast.framework.kit.StrKit;
import com.jfast.framework.web.annotation.ConfigurationProperties;

@ConfigurationProperties(prefix="jfast.dataSource")
public class DataSourceConfig {

	public static final String TYPE_MYSQL = "mysql";
    public static final String TYPE_ORACLE = "oracle";
	public static final String TYPE_SQLSERVER = "sqlserver";
	public static final String TYPE_SQLITE = "sqlite";
	
	private static final String DEFAULT_DATASOURCE_FACTORY = "com.jfast.framework.orm.dataSource.DefaultDataSourceFactory";
	    
	private String type = TYPE_MYSQL;
	private String name = DataSourceManager.DEFAULT_DATASOURCE_NAME;
	private String url;
	private String mapperLocation;
	private String password;
	private String username;
	private String driverClassName = "com.mysql.jdbc.Driver";
	private String dataSourceClassName = DEFAULT_DATASOURCE_FACTORY;//数据源类名称
	private boolean showSql = true;
	private String basePackage; //实体类包名
	// 初始连接池大小、最小空闲连接数、最大活跃连接数
	private int initialSize = 10;
	private int minIdle = 10;
	private int maxActive = 100;
	private int maxWait = DruidDataSource.DEFAULT_MAX_WAIT;
	// 配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒
	@SuppressWarnings("unused")
	private long timeBetweenEvictionRunsMillis = DruidDataSource.DEFAULT_TIME_BETWEEN_EVICTION_RUNS_MILLIS;
	// 配置连接在池中最小生存的时间
	@SuppressWarnings("unused")
	private long minEvictableIdleTimeMillis = DruidDataSource.DEFAULT_MIN_EVICTABLE_IDLE_TIME_MILLIS;
	// 配置发生错误时多久重连
	@SuppressWarnings("unused")
	private long timeBetweenConnectErrorMillis = DruidDataSource.DEFAULT_TIME_BETWEEN_CONNECT_ERROR_MILLIS;
	
	
	public void setMaxWait(int maxWait) {
		this.maxWait = maxWait;
	}
	
	public int getMaxWait() {
		return maxWait;
	}
	
	
	public int getInitialSize() {
		return initialSize;
	}

	public void setInitialSize(int initialSize) {
		this.initialSize = initialSize;
	}

	public int getMinIdle() {
		return minIdle;
	}

	public void setMinIdle(int minIdle) {
		this.minIdle = minIdle;
	}

	public int getMaxActive() {
		return maxActive;
	}

	public void setMaxActive(int maxActive) {
		this.maxActive = maxActive;
	}

	
	public String getBasePackage() {
		return basePackage;
	}

	public void setBasePackage(String basePackage) {
		this.basePackage = basePackage;
	}

	public boolean isShowSql() {
		return showSql;
	}

	public void setShowSql(boolean showSql) {
		this.showSql = showSql;
	}

	public String getDataSourceClassName() {
		return dataSourceClassName;
	}

	public void setDataSourceClassName(String dataSourceClassName) {
		this.dataSourceClassName = dataSourceClassName;
	}
	 
	public boolean isOk() {
		return StrKit.isNotEmpty(url) && StrKit.isNotEmpty(username);
	}

	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getDriverClassName() {
		return driverClassName;
	}
	
	public void setDriverClassName(String driverClassName) {
		this.driverClassName = driverClassName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setMapperLocation(String mapperLocation) {
		this.mapperLocation = mapperLocation;
	}

	public String getMapperLocation() {
		return mapperLocation;
	}
}
