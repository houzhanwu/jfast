package com.jfast.framework.orm.query;

import com.jfast.framework.orm.Page;
import com.jfast.framework.orm.config.DbConfig;
import java.util.List;
import java.util.Map;

public interface JdbcTemplate {

    public <T> T findOne(String sql, Class<T> clazz);

    public <T> T findOne(String sql, Class<T> clazz, Object... params);

    public  Map<String,Object>  findMap(String sql, String configName);
    
    public  Map<String,Object>  findMap(String sql);
    
    public  Map<String, Object> findMap(String sql, Object... params);

    public  Map<String,Object>  findMap(String sql, String configName, Object... params);

    public <T> List<T> findAll(String sql, Class<T> clazz);

    public <T> List<T> findAll(String sql, Class<T> clazz, Object... params);

    public boolean insert(Object object);

    public boolean update(Object object);

    public <T> Page<T> paginate(int pageNumber, int pageSize, String select, String sqlContent, Class<T> clazz, Object... params);

    public DbConfig getDefaultDbConfig();

    public DbConfig getDbConfig(String configName);

//    public void commit(boolean autoCommit);
//
//    public void commit();
//
//    public void rollback();
//
//    public void colse();
}
