/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.orm.dataSource;

import com.jfast.framework.orm.config.DataSourceConfig;

import javax.sql.DataSource;

public interface DataSourceFactory {

	/**
	 * 获取DataSource
	 * @return
	 */
	public DataSource getDataSource();
	
	/**
	 * 获取数据源名称
	 * @return
	 */
	public String getConfigName();
	
	
	/**
	 * 获取Table 对应数据源名称(主要用于多源数据库)
	 * @return
	 */
	public DataSourceConfig getDataSourceConfig();
}
