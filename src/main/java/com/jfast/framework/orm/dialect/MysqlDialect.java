/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.orm.dialect;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class MysqlDialect extends Dialect {
	
	@Override
	public void forModelSave(StringBuilder sql, Map<String,Object> attrs, String tableName, List<Object> paras){
		sql.append("insert into `").append(tableName).append("`(");
		StringBuilder temp = new StringBuilder(") values(");
		for (Entry<String, Object> e : attrs.entrySet()) {
			String colName = e.getKey();
			if(paras.size() > 0){
				sql.append(",");
				temp.append(",");
			}
			sql.append('`').append(colName).append('`');
			temp.append('?');
			paras.add(e.getValue());
		}
		sql.append(temp).append(")");
	}

	@Override
	public String forPaginate(int pageNumber, int pageSize, String findSql) {
		StringBuilder builder = new StringBuilder();
		int offset = pageSize * (pageNumber - 1);
		builder.append(findSql).append(" limit ").append(offset).append(", ").append(pageSize);	// limit can use one or two '?' to pass paras
		return builder.toString();
	}

	@Override
	public void forModelUpdate(StringBuilder sql, Map<String, Object> attrs, String tableName, List<Object> paras) {
		
	}
}
