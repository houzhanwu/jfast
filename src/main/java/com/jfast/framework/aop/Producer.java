/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.aop;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import net.sf.cglib.proxy.Enhancer;

/**
 * 代理类建造者
 * @author zengjintao
 * @version 1.0
 * @createTime 2017年10月29日下午1:15:37
 */
public class Producer {

	private static final Map<String, Object> singletons = new ConcurrentHashMap<>();
	
	@SuppressWarnings("rawtypes")
	public static Object create(Class targetClass) {
		return Enhancer.create(targetClass, new JfastCallBack());
	}
	
	/**
	 * 单例
	 * @param className
	 * @param targetClass
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static Object create(String className, Class targetClass) {
		Object object = singletons.get(className);
		if (object == null) {
			synchronized (targetClass) {
				object = singletons.get(className);
				if(object == null){
                    object = Enhancer.create(targetClass, new JfastCallBack());
					singletons.put(className, object);
				}
			}
		}
		return object;
	}
}
