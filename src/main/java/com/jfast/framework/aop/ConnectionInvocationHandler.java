package com.jfast.framework.aop;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.sql.Connection;

/**
 * 
 * @author zengjintao
 * @version 1.0
 * @createTime 2018年7月12日上午11:26:51
 */
public class ConnectionInvocationHandler<T> implements InvocationHandler {

	private T target; //被代理的目标对象
	
	public ConnectionInvocationHandler(T target) {
		this.target = target;
	}
	
	@SuppressWarnings("unchecked")
	public T getTarget() {
		Class<?> clazz = target.getClass();
		return (T) Proxy.newProxyInstance(clazz.getClassLoader(), target.getClass().getInterfaces(), this);
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		if (proxy instanceof Connection && method.getName().equals("prepareStatement")) {
			System.out.println("sql: " + args[0]);
		}
		return method.invoke(target, args);
	}
}
