/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.exception;

/**
 * 数据源异常
 * @author zengjintao
 * @version 1.0
 * @createTime 2018年2月28日上午10:51:20
 */
public class DataSourceException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public DataSourceException() {
		super();
	}
	
	public DataSourceException(String message) {
		super(message);
	}
	
	public DataSourceException(Throwable throwable) {
		super(throwable);
	}
	
	public DataSourceException(String message, Throwable throwable) {
		super(message,throwable);
	}
}