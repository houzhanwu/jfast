package com.jfast.framework.bean;

import com.jfast.framework.Jfast;
import java.util.List;

/**
 * guice bean实例
 * @author zengjintao
 * @version 1.0
 * @create 2018/10/8 17:09
 **/
@SuppressWarnings("serial")
public class GuiceClassFactory extends AbstractClassFactory {

    private ClassFactory delegate = new JdkClassFactory();

    @SuppressWarnings("unchecked")
	@Override
    public <T> T createBean(Class<T> classType) {
        Class<?> createClass = parseInterface(classType);
        return (T) Jfast.getBean(createClass);
    }

    @Override
    public <T> T createBean(Class<T> classType, List<Object> params) {
        return delegate.createBean(classType, params);
    }
}
