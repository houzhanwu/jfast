/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.web.session;

import com.jfast.framework.web.cache.JfastCache;
import com.jfast.framework.web.cache.JfastCacheManager;
import com.jfast.framework.web.config.JfastConfig;
import com.jfast.framework.web.config.JfastConfigManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpSession;

/**
 * @author wangxue
 * @version 1.0
 * @createTime 2017年10月28日下午2:46:59
 */
public class JfastHttpServletRequest extends HttpServletRequestWrapper {

    private HttpSession httpSession;

    /**
     * @param request
     */
    public JfastHttpServletRequest(HttpServletRequest request) {
        super(request);
    }

    @Override
    public HttpSession getSession() {
        return getSession(true);
    }

    @Override
    public HttpSession getSession(boolean create) {
        if (httpSession == null) {
            JfastConfig config = JfastConfigManager.createConfigBean(JfastConfig.class);
            if(config.hasCache()){
                JfastCache jfastCache = JfastCacheManager.getCacheManager().getJfastCache(config);
                httpSession = new JfastHttpSession(jfastCache);
            }else{
                httpSession = super.getSession(create);
            }
        }
        return httpSession;
    }
}
