package com.jfast.framework.web.resolver;

import com.jfinal.template.Engine;

/**
 * jfinal enjoy模板引擎
 * @author zengjintao
 * @version 1.0
 * @createTime 2017年12月5日下午2:34:23
 */
public class EnjoyResolver extends ViewResolver {

	@SuppressWarnings("unused")
	private Engine engine;
	
	@Override
	public void render() {
		
	}

}
