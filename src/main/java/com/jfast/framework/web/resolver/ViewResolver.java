/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.web.resolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jfast.framework.Jfast;
import com.jfast.framework.web.config.JfastConfig;
import com.jfast.framework.web.core.JfastConstant;



public abstract class ViewResolver {

	private static String encoding = JfastConstant.ENCODING_DEFAULT;
	private Object returnValue;

	protected HttpServletRequest request;
	protected HttpServletResponse response;
	protected JfastConfig config;
	
	public ViewResolver setContext(HttpServletRequest request, HttpServletResponse response) {
		this.request = request;
		this.response = response;
		this.config = Jfast.getJfastConfig();
		return this;
	}
	
	public ViewResolver setContext(Object returnValue, HttpServletRequest request, HttpServletResponse response) {
		this.request = request;
		this.response = response;
		this.returnValue = returnValue;
		this.config = Jfast.getJfastConfig();
		return this;
	}
	
	public static String getEncoding() {
		return encoding;
	}

	public static void setEncoding(String encoding) {
		ViewResolver.encoding = encoding;
	}
	
	public Object getReturnValue() {
		if (returnValue instanceof String && !((String) returnValue).startsWith("/")) {
			returnValue = "/" + returnValue;
		}
		return returnValue;
	}

	public void setReturnValue(Object returnValue) {
		this.returnValue = returnValue;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	public abstract void render();
	
}
