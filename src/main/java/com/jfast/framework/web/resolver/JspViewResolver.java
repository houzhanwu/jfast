/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.web.resolver;


import java.io.IOException;

import javax.servlet.ServletException;
import com.jfast.framework.exception.ResolverException;

/**
 * jsp视图解析器
 * @version 1.0
 */
public class JspViewResolver extends ViewResolver {

	@Override
	public void render() {
	    try {
			request.getRequestDispatcher(config.getBaseViewPath() + (String)getReturnValue() + config.getSuffix()).forward(request, response);
		} catch (ServletException | IOException e) {
			throw new ResolverException(e);
		}
	}
}
