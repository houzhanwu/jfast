/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.web.core;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 请求方法映射
 * @since 1.1
 */
public class RequestMethodMapping {

	private String controllerKey; //controller映射
	private String methodMapping; // 方法映射
	private Method method; //接口方法
	private Class<?> controllerClass;
    private RequestMethod[] requestType; //http请求类型
	private final Map pathVariableMap = new HashMap(); //restful 参数
	private final List<String> pathVariableParams;

	public void setRequestType(RequestMethod[] requestType) {
		this.requestType = requestType;
	}

	public RequestMethod[] getRequestType() {
		return requestType;
	}
	
	public Class<?> getControllerClass() {
		return controllerClass;
	}

	public Method getMethod() {
		return method;
	}

	public void setMethod(Method method) {
		this.method = method;
	}

	public String getMethodMapping() {
		return methodMapping;
	}

	public void setMethodMapping(String methodMapping) {
		this.methodMapping = methodMapping;
	}

	public void setControllerClass(Class<?> controllerClass) {
		this.controllerClass = controllerClass;
	}

	public RequestMethodMapping(String controllerKey, String methodMapping, Method method,
								Class<?> controllerClass, List<String> pathVariableParams, RequestMethod[] requestType) {
		this.controllerKey = controllerKey;
		this.methodMapping = methodMapping;
		this.method = method;
		this.controllerClass = controllerClass;
		this.pathVariableParams = pathVariableParams;
		this.requestType = requestType;
	}

	public List<String> getPathVariableParams() {
		return pathVariableParams;
	}

	public Map getPathVariableMap() {
		return pathVariableMap;
	}

	public String getControllerKey() {
		return controllerKey;
	}

	public void setControllerKey(String controllerKey) {
		this.controllerKey = controllerKey;
	}
}
