package com.jfast.framework.web.core.param;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.jfast.framework.web.session.RequestContext;

public class Model {
	
    private HttpServletRequest request = RequestContext.getRequestManager().getRequest();
	
	public void setAttr(String key, Object value){
		request.setAttribute(key, value);
	}
	
	@SuppressWarnings("unchecked")
	public <T> T getAttr(String name){
		return (T) request.getAttribute(name);
	}
	
	public void removeAttr(String key, Object value){
		request.removeAttribute(key);
	}
	
	@SuppressWarnings("unchecked")
	public <T>T  getSessionAttr(String name){
		return (T) getSession().getAttribute(name);
	}
	
	public void setSessionAttr(String key, Object value){
		getSession().setAttribute(key, value);
	}
	
	public void removeSessionAttr(String name){
		getSession().removeAttribute(name);
	}
	
	public HttpSession getSession(){
		return request.getSession();
	}
}
