package com.jfast.framework.web.core;

import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import com.jfast.framework.util.DateUtil;

class RequestMappingReport {
	
	public static void report(String target, RequestMethodMapping requestMethodMapping, HttpServletRequest request){
		StringBuilder context = new StringBuilder();
		context.append("\n-------- Jfast action report -------- " + DateUtil.getSecondDate(new Date()));
		context.append("\nURL         " + request.getMethod() + "  " + requestMethodMapping.getMethodMapping());
		context.append("\nController  " + requestMethodMapping.getControllerClass());
		context.append("\nMethod      " + requestMethodMapping.getMethod().getName());
		System.out.println(context.toString()); 
	}
}

