package com.jfast.framework.web.core.param;

import java.util.Map;
import java.util.Map.Entry;
import javax.servlet.http.HttpServletRequest;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;


public class MapParam<K, V> extends RequestMappingMethodParam {

	public MapParam(HttpServletRequest request){
		super(request);
	}

	@Override
	Object doGetBean(Map<String, String[]> params) {
		ModelMap<String, V> requestMap = new ModelMap<String, V>();
		for (Entry<String, String[]> entry : params.entrySet()) {
			String paramKey = entry.getKey();
			if (entry.getValue().length == 1) {
				requestMap.put(paramKey, (V)entry.getValue()[0]);
			}
			else {
				requestMap.put(paramKey, (V) StringUtils.join(entry.getValue(), ","));
			}
		}
		return requestMap;
	}

	@Override
	Object doGetBeanByJson(String requestBody) {
		return JSONObject.parse(requestBody);
	}

}
