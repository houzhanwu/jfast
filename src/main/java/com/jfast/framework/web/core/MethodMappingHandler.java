/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.web.core;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jfast.framework.Jfast;
import com.jfast.framework.aop.InterceptorManager;
import com.jfast.framework.aop.Invocation;
import com.jfast.framework.exception.ResolverException;
import com.jfast.framework.log.Logger;
import com.jfast.framework.web.handler.Handler;
import com.jfast.framework.web.resolver.ViewFactory;
import com.jfast.framework.web.resolver.ViewResolver;

/**
 * @since 1.1
 */
public class MethodMappingHandler extends Handler {

	private static final Logger logger = Logger.getLogger(MethodMappingHandler.class);
	private static final MethodMappingHandler methodMappingHandler = new MethodMappingHandler();
	private static final InterceptorManager interceptorManager = InterceptorManager.getInteceptorManager();

    private MethodMappingHandler() {
		
	}

	public static MethodMappingHandler getMethodMappingHandler() {
		return methodMappingHandler;
	}

	@Override
	public void doHandler(String target, HttpServletRequest request, HttpServletResponse response) {
		RequestMethodMapping requestMethodMapping = RequestMethodMappingManager.getRequestMethodMappingManager()
				.getRequestMethodMapping(target);
		if (requestMethodMapping == null) {
			if (logger.isWarnEnabled()) {
				String qs = request.getQueryString();
				logger.warn("404 requestMethodMapping Not Found: " + (qs == null ? target : target + "?" + qs));
			}
		}
		try {
			Object controller = Jfast.getBean(requestMethodMapping.getControllerClass());
			Jfast.injectMembers(controller);//guice依赖注入
			Invocation invocation =	new Invocation(requestMethodMapping, controller,
					interceptorManager.getInterceptorsToArray(), request, response);
    		invocation.invoke(request, response);
    		RequestMappingReport.report(target, requestMethodMapping, request);
			Object returnValue = invocation.getReturnValue();
			ViewResolver viewResolver = ViewFactory.getViewFactory().getViewResolver(requestMethodMapping.getMethod(), returnValue);
			viewResolver.setContext(returnValue, request, response).render();
		} catch(ResolverException e) {
			if (logger.isErrorEnabled()) {
				String qs = request.getQueryString();
				logger.error(qs == null ? target : target + "?" + qs, e);
			}
		}
		catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
}
