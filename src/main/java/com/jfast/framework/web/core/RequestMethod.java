package com.jfast.framework.web.core;

/**
 * http请求类型
 * @author zengjintao
 * @version 1.0
 * @createTime 2018年9月1日下午6:36:50
 */
public enum RequestMethod {
	
	GET, HEAD, POST, PUT, DELETE
}
