/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.web.core.param;

import java.util.ArrayList;
import java.util.List;

/**
 * 用于注入action参数注入
 * @author zengjintao
 * @version 1.0
 * @createTime 2018年1月9日下午8:55:37
 */
public class ParamProcessor {
	
	private List<MethodParam> methodParas = new ArrayList<>();
	
	public List<MethodParam> getMethodParas() {
		return methodParas;
	}

	public void addMethodParam(MethodParam methodPara) {
		if (methodParas != null)
		   methodParas.add(methodPara);
	}
	
	public void removeMethodParam(MethodParam methodPara) {
		if (methodParas != null)
		   methodParas.remove(methodPara);
	}
	
	public Object[] getParams() {
		List<Object> params = new ArrayList<>();
		for (MethodParam methodPara : methodParas) {
			params.add(methodPara.getParam());
		}
		return params.toArray();
	}
}
