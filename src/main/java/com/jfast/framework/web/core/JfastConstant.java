/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.web.core;


/**
 * Jfast常量定义
 * @author zengjintao
 * @version 1.0
 * @createTime 2017年12月24日下午6:24:47
 */
public final class JfastConstant {

	public static final String ENCODING_DEFAULT = "UTF-8";
	
	public static final String SEPARATOR = "/";
	
	public static final String TRUE = "1"; //是
	
	public static final String FALSE = "0"; //否
	
	/**
	 * jast视图类型
	 * @author zengjintao
	 * @version 1.0
	 * @createTime 2018年1月5日下午8:33:21
	 */
	public enum ViewType {
		JSP, VELOCITY, JFINAL_VIEW;
	}
	
	/**
	 * 类功能说明:bean的范围，单例，原型，会话，以及线程
	 * @author zengjintao
	 * @version 1.0
	 * @createTime 2018年1月5日下午8:33:17
	 */
	public enum BeanScope {
		SINGLETON, PROTOTYPE;
	}
	
	/**
	 * bean类型  
	 * PROXY 代理类
	 * @author taoge
	 * @version 1.0
	 * @create_at 2018年10月1日下午7:59:40
	 */
	public enum BeanType {
		PROXY, DEFAAULT;
	}
}
