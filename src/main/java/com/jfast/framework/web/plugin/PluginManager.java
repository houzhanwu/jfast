/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.web.plugin;

import java.util.ArrayList;
import java.util.List;

import com.jfast.framework.Jfast;

/**
 * 插件管理器
 * @author zengjintao
 * @version 1.0
 */
public class PluginManager {

	private static final PluginManager pluginManger = new PluginManager();
	
	private static final List<IPlugin> plugins = new ArrayList<>();
	
	private PluginManager() {
		
	}
	
	public static PluginManager getPluginManger() {
		return pluginManger;
	}
	
	public PluginManager add(IPlugin plugin){
		Jfast.injectMembers(plugin);//方便为plugin插件的自动注入功能
		plugins.add(plugin);
		return this;
	}
	
	public List<IPlugin> getPlugins() {
		return plugins;
	}
}
