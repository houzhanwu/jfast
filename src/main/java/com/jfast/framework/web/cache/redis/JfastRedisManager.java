package com.jfast.framework.web.cache.redis;

import com.jfast.framework.web.config.JfastConfigManager;

public class JfastRedisManager {

	private static final JfastRedisManager jfastRedisManager = new JfastRedisManager();
	
	private static JfastRedis jfastRedis = null;
	
	private JfastRedisManager(){
		
	}
	
	public static JfastRedisManager getJfastRedisManager() {
		return jfastRedisManager;
	}
	
	public JfastRedis getRedis(){
		if(jfastRedis == null){
			JfastRedisConfig config = JfastConfigManager.createConfigBean(JfastRedisConfig.class);
			jfastRedis = getRedis(config);
		}
		return jfastRedis;
	}

	private JfastRedis getRedis(JfastRedisConfig config) {
		return new JfastRedis(config);
	}
}
