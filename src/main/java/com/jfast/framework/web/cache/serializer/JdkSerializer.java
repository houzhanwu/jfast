/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.web.cache.serializer;



import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import redis.clients.util.SafeEncoder;

/**
 * JdkSerializer
 * @author zengjintao
 * @version 1.0
 * @createTime 2017年11月3日下午3:31:52
 */
public class JdkSerializer implements ISerializer {

	/**
	 * 对象转换成bytes数组
	 */
	@Override
	public byte[] valueToBytes(Object value) {
		ObjectOutputStream objectOut = null;
		try {
			ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
			objectOut = new ObjectOutputStream(byteOut);
			objectOut.writeObject(value);
			objectOut.flush();
			return byteOut.toByteArray();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}finally {
			if(objectOut != null){
				try {
					objectOut.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	@Override
	public Object bytesToObject(byte[] bytes){
		if(bytes == null || bytes.length == 0)
			return null;
		
		ObjectInputStream objectIn = null;
		try {
			ByteArrayInputStream byteIn = new ByteArrayInputStream(bytes);
			objectIn = new ObjectInputStream(byteIn);
			return objectIn.readObject();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}finally {
			if(objectIn != null){
				try {
					objectIn.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public byte[] keyToBytes(String key) {
		return SafeEncoder.encode(key);
	}
}
