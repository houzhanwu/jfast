package com.jfast.framework.web.cache.redis;

import com.jfast.framework.web.cache.JfastCache;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class JfastRedisCache extends JfastCache {

    private JfastRedis jfastRedis;

    public JfastRedisCache(){
        this.jfastRedis = JfastRedisManager.getJfastRedisManager().getRedis();
        if(jfastRedis == null){
            throw  new NullPointerException("can not get jfastRedis,please correct config jfast.cache.redis.host or jfast.redis.host");
        }
    }

    @Override
    public void put(String cacheName, Object key, Object value) {
        String newKey = buildKey(cacheName,key);
        jfastRedis.set(newKey,value);
    }

    @Override
    public void set(String cacheName, Object key, Object value) {
        put(cacheName, key, value);
    }

    @Override
    public <T> T get(String cacheName, Object key) {
        String newKey = buildKey(cacheName,key);
        return jfastRedis.get(newKey);
    }

    @SuppressWarnings("rawtypes")
	@Override
    public List getKeys(String cacheName) {
        Set<String> keys = jfastRedis.keys(":*");
        if(keys == null || keys.isEmpty()){
            return null;
        }
        List<Object> keyList = new ArrayList<>();
        for (String key : keys) {
        	keyList.add(key);
		}
        return keyList;
    }

    @Override
    public void remove(String cacheName, Object key) {
         String newKey = buildKey(cacheName, key);
         jfastRedis.del(newKey);
    }
}
