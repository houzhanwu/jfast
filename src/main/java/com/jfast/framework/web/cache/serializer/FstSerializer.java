/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.web.cache.serializer;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.nustaq.serialization.FSTObjectInput;
import org.nustaq.serialization.FSTObjectOutput;

import redis.clients.util.SafeEncoder;

/**
 * FstSerializer
 * @author zengjintao
 * @version 1.0
 * @createTime 2017年11月3日下午3:31:43
 */
public class FstSerializer implements ISerializer {

	@Override
	public byte[] valueToBytes(Object value) {
		FSTObjectOutput fstOut = null;
		try {
			ByteArrayOutputStream bytesOut = new ByteArrayOutputStream();
			fstOut = new FSTObjectOutput(bytesOut);
			fstOut.writeObject(value);
			fstOut.flush();
			return bytesOut.toByteArray();
		} 
		catch (Exception e) {
			throw new RuntimeException(e);
		}
		finally {
			if(fstOut != null)
				try {fstOut.close();} catch (IOException e) {e.printStackTrace();}
		}
	}
	
	@Override
	public Object bytesToObject(byte[] bytes){
		if(bytes == null || bytes.length == 0)
			return null;
		
		FSTObjectInput fstIn = null;
		try {
			ByteArrayInputStream bytesIn = new ByteArrayInputStream(bytes);
			fstIn = new FSTObjectInput(bytesIn);
			return fstIn.readObject();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}finally {
			if(fstIn != null){
				try {fstIn.close();} catch (IOException e) {e.printStackTrace();}
			}
		}
	}

	@Override
	public byte[] keyToBytes(String key) {
	    return SafeEncoder.encode(key);
	}
}
