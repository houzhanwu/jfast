package com.jfast.framework.web.cache.serializer;

import com.jfast.framework.web.annotation.ConfigurationProperties;

@ConfigurationProperties(prefix = "jfast.serializer")
public class JfastSerializerConfig {

	 public static final String FST = "fst";
     public static final String JDK = "jdk";
     
     public String type = FST;

     public String getType() {
         return type;
     }

     public void setType(String type) {
         this.type = type;
     }
}
