/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.web.cache.redis;


import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.jfast.framework.kit.StrKit;
import com.jfast.framework.log.Logger;
import com.jfast.framework.web.cache.serializer.ISerializer;
import com.jfast.framework.web.cache.serializer.ISerializerFactory;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisPubSub;

public class JfastRedis {

	private static final Logger logger = Logger.getLogger(JfastRedis.class);
	private JedisPool jedisPool;
	private static final ThreadLocal<Jedis> threadLocal = new ThreadLocal<Jedis>();
	private ISerializer iSerializer;
	private JfastRedisConfig jfastRedisConfig;
	
	public JfastRedis(JfastRedisConfig jfastRedisConfig) {
		this.jfastRedisConfig = jfastRedisConfig;
		this.jedisPool = newJedisPoll(jfastRedisConfig);
		this.iSerializer = ISerializerFactory.getISerializerFactory().getISerializer();
	}
	
	private JedisPool newJedisPoll(JfastRedisConfig jfastRedisConfig) {
		
		Integer port = jfastRedisConfig.getPort();
		String password = jfastRedisConfig.getPassword();
		String host = jfastRedisConfig.getHost();
		Integer maxIdle = jfastRedisConfig.getMaxIdle();
		Integer maxTotal = jfastRedisConfig.getMaxTotal();
		Integer minIdle = jfastRedisConfig.getMinIdle();
		Integer maxWaitMillis = jfastRedisConfig.getMaxWaitMillis();
		Integer timeout = jfastRedisConfig.getTimeOut();
		
		JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
		
		if (StrKit.isNotEmpty(maxWaitMillis)) {
			jedisPoolConfig.setMaxWaitMillis(maxWaitMillis);
		}
		
		if (StrKit.isNotEmpty(maxIdle)) {
			jedisPoolConfig.setMaxIdle(maxIdle);
		}
		
		if (StrKit.isNotEmpty(minIdle)) {
			jedisPoolConfig.setMinIdle(minIdle);
		}
		
		if (StrKit.isNotEmpty(maxWaitMillis)) {
			jedisPoolConfig.setMaxWaitMillis(maxWaitMillis);
		}
		
		if (StrKit.isNotEmpty(maxTotal)) {
			jedisPoolConfig.setMaxTotal(maxTotal);
		}
		
		if (port != null && host != null) {
			jedisPool = new JedisPool(jedisPoolConfig, host, port, timeout, password);
		}
		
		if (port != null && host != null && password != null && timeout != null) {
			jedisPool = new JedisPool(jedisPoolConfig, host, port, timeout, password);
		}
		
		
		return jedisPool ;
	}

	public JfastRedis(ISerializer iSerializer,JedisPool jedisPool) {
		this.iSerializer = iSerializer;
		this.jedisPool = jedisPool;
	}
	
	
	/**
	 * 确认一个key是否存在
	 * @param key
	 * @return
	 */
	public boolean exists(Object key) {
		Jedis jedis = getJedis();
		try {
			return jedis.exists(valueToBytes(key));
		} finally {
			close(jedis);
		}
	}
	
	/**
	 * 删除元素
	 * @param key
	 * @return
	 */
	public Long del(Object key) {
		Jedis jedis = getJedis();
		try {
			return jedis.del(valueToBytes(key));
		} finally {
			close(jedis);
		}
	}
	
	/**
	 * 随机返回key空间的一个key
	 * @return
	 */
	public String randomkey() {
		Jedis jedis = getJedis();
		try {
			return jedis.randomKey();
		} finally {
			close(jedis);
		}
	}
	
	/**
	 * 将key由oldname重命名为newname，若newname存在则删除newname表示的key
	 * @param oldkey
	 * @param newkey
	 * @return
	 */
	public String rename(Object oldkey, Object newkey) {
		Jedis jedis = getJedis();
		try {
			return jedis.rename(valueToBytes(oldkey), valueToBytes(newkey));
		} finally {
			close(jedis);
		}
	}
	
	/**
	 * 返回当前数据库中key的数目
	 * @return
	 */
	public Long dbsize() {
		Jedis jedis = getJedis();
		try {
			return jedis.dbSize();
		} finally {
			close(jedis);
		}
	}
	
	 /**
     * 为给定 key 设置生存时间，当 key 过期时(生存时间为 0 )，它会被自动删除。
     * 
     */
    public Long expire(Object key, int seconds) {
        Jedis jedis = getJedis();
        try {
            return jedis.expire(valueToBytes(key), seconds);
        } finally {
        	close(jedis);
        }
    }

    /**
     * 获得一个key的活动时间
     * @param key
     * @return
     */
	public Long ttl(Object key) {
		Jedis jedis = getJedis();
        try {
            return jedis.ttl(valueToBytes(key));
        } finally {
        	close(jedis);
        }
	}
	
	/**
	 * 按索引查询
	 * @param databaseIndex
	 * @return
	 */
	public String select(int databaseIndex) {
        Jedis jedis = getJedis();
        try {
            return jedis.select(databaseIndex);
        } finally {
        	close(jedis);
        }
    }
	
	/**
	 * 将当前数据库中的key转移到有dbindex索引的数据库
	 * @param key
	 * @param dbIndex
	 * @return
	 */
	public Long move(Object key,int dbIndex) {
		Jedis jedis = getJedis();
        try {
            return jedis.move(valueToBytes(key), dbIndex);
        } finally {
        	close(jedis);
        }
	}
	
	/**
	 * 删除当前选择数据库中的所有key
	 * @return
	 */
	public String flushDb() {
		Jedis jedis = getJedis();
		try {
			return jedis.flushDB();
		} finally {
			close(jedis);
		}
	}
	
	/**
	 * 删除所有数据库中的所有key
	 * @return
	 */
	public String flushAll() {
		Jedis jedis = getJedis();
		try {
			return jedis.flushAll();
		} finally {
			close(jedis);
		}
	}
	
	
	/**
	 * 给数据库中名称为key的string赋予值value
	 * @param key
	 * @param value
	 * @return
	 */
	public String set(Object key,Object value) {
		Jedis jedis = getJedis();
		try {
			return jedis.set(valueToBytes(key), valueToBytes(value));
		}finally {
			close(jedis);
		}
	}
	
	/**
	 * 给名称为key的string赋予上一次的value
	 * @param key
	 * @param value
	 */
	public void getset(Object key, Object value) {
		Jedis jedis = getJedis();
		try {
			jedis.getSet(valueToBytes(key), valueToBytes(value));
		}finally {
			close(jedis);
		}
	}
	
	
	/**
	 * 返回库中多个value （它们的名称为key1，key2…）的value
	 * @param keys
	 */
	public List<Object> mget(Object... keys) {
		Jedis jedis = getJedis();
		try {
			List<byte[]> data = jedis.mget(vauleToByteArray(keys));
			return bytesListToValueList(data);
		}finally {
			close(jedis);
		}
	}
	
	/**
	 * 如果不存在名称为key的string，则向库中添加string，名称为key，值为value
	 * @param key
	 * @param value
	 */
	public Long setnx(Object key, Object value) {
		Jedis jedis = getJedis();
		try {
			return jedis.setnx(valueToBytes(key), valueToBytes(key));
		}finally {
			close(jedis);
		}
	}
	
	/**
	 * 向库中添加string（名称为key，值为value）同时，设定过期时间seconds
	 * @param key
	 * @param seconds
	 * @param value
	 * @return
	 */
	public String setex(Object key,Object value,int seconds) {
		Jedis jedis = getJedis();
		try {
			return jedis.setex(valueToBytes(key), seconds, valueToBytes(value));
		}finally {
			close(jedis);
		}
	}
	
	
	/**
     * 同时设置一个或多个 key-value 对。
     * 如果某个给定 key 已经存在，那么 MSET 会用新值覆盖原来的旧值，如果这不是你所希望的效果，请考虑使用 MSETNX 命令：它只会在所有给定 key 都不存在的情况下进行设置操作。
     * MSET 是一个原子性(atomic)操作，所有给定 key 都会在同一时间内被设置，某些给定 key 被更新而另一些给定 key 没有改变的情况，不可能发生。
     * <pre>
     * 例子：
     * Cache cache = RedisKit.use();			// 使用 JbootRedis 的 cache
     * cache.mset("k1", "v1", "k2", "v2");		// 放入多个 key value 键值对
     * List list = cache.mget("k1", "k2");		// 利用多个键值得到上面代码放入的值
     * </pre>
	 * @return 
     */
	public String mset(Object... keysValues) {
		if(keysValues.length % 2 != 0) {
			throw new IllegalArgumentException("wrong number of arguments for met, keysValues length can not be odd");
		}
		Jedis jedis = getJedis();
		try {
			byte[][] kv = new byte[keysValues.length][];
			for (int i = 0; i < keysValues.length; i++) {
				if(i % 2 == 0)
					kv[i] = valueToBytes(keysValues[i]);
				else
					kv[i] = keyToBytes((String)keysValues[i]);
			}
			return jedis.mset(kv);
		} finally {
			close(jedis);
		}
	}
	
	/**
	 * 名称为key的string增1操作
	 * @param key
	 */
	public Long incr(Object key) {
		Jedis jedis = getJedis();
		try {
			return jedis.incr(valueToBytes(key));
		}finally {
			close(jedis);
		}
	}
	
	/**
	 * 名称为key的string增加number
	 * @param key
	 * @param number
	 * @return
	 */
	public Long incrby(Object key, Long number) {
		Jedis jedis = getJedis();
		try {
			return jedis.incrBy(valueToBytes(key), number);
		}finally {
			close(jedis);
		}
	}
	
	/**
	 * 名称为key的string减1操作
	 * @param key
	 * @return
	 */
	public Long decr(Object key) {
		Jedis jedis = getJedis();
		try {
			return jedis.decr(valueToBytes(key));
		}finally {
			close(jedis);
		}
	}
	
	/**
	 * 名称为key的string减少number
	 * @param key
	 * @param number
	 * @return
	 */
	public Long decrBy(Object key, Long number) {
		Jedis jedis = getJedis();
		try {
			return jedis.decrBy(valueToBytes(key),number);
		}finally {
			close(jedis);
		}
	}
	
	/**
	 * 名称为key的string的值附加value
	 * @param key
	 * @param value
	 * @return
	 */
	public Long append(Object key,Object value) {
		Jedis jedis = getJedis();
		try {
			return jedis.append(valueToBytes(key), valueToBytes(value));
		}finally {
			close(jedis);
		}
	}
	
	/**
	 * 返回名称为key的string的value的子串
	 * @return 
	 */
	public byte[] substr(Object key,int start, int end) {
		Jedis jedis = getJedis();
		try {
			return jedis.substr(valueToBytes(key), start, end);
		}finally {
			close(jedis);
		}
	}
	
	/**
	 * 在名称为key的list尾添加一个值为value的元素
	 * @param key
	 * @param value
	 * @return 
	 */
	public Long rpush(Object key,Object value) {
		Jedis jedis = getJedis();
		try {
			return jedis.rpush(valueToBytes(key), valueToBytes(value));
		}finally {
			close(jedis);
		}
	}
	
	public String watch(Object key) {
		Jedis jedis = getJedis();
		try {
			return jedis.watch(valueToBytes(key));
		}finally {
			close(jedis);
		}
	}
	
	public String watch(String... keys) {
		Jedis jedis = getJedis();
		try {
			return jedis.watch(keys);
		}finally {
			close(jedis);
		}
	}
	
	/**
	 * 在名称为key的list尾添加一个值为value的元素
	 * @param key
	 * @param value
	 * @return 
	 */
	public Long rpushx(Object key,Object... value) {
		Jedis jedis = getJedis();
		try {
			return jedis.rpushx(valueToBytes(key), valueToBytes(value));
		}finally {
			close(jedis);
		}
	}
	
	/**
	 * 在名称为key的list头添加一个值为value的 元素
	 * @param key
	 * @param value
	 * @return
	 */
	public Long lpush(Object key,Object value) {
		Jedis jedis = getJedis();
		try {
			return jedis.lpush(valueToBytes(key), valueToBytes(value));
		}finally {
			close(jedis);
		}
	}
	
	/**
	 * 在名称为key的list头添加一个值为value的 元素
	 * @param key
	 * @param value
	 * @return
	 */
	public Long lpushx(Object key,Object... value) {
		Jedis jedis = getJedis();
		try {
			return jedis.lpushx(valueToBytes(key), valueToBytes(value));
		}finally {
			close(jedis);
		}
	}
	
	/**
	 * 返回名称为key的list的长度
	 * @param key
	 * @return
	 */
	public Long llen(Object key) {
		Jedis jedis = getJedis();
		try {
			return jedis.llen(valueToBytes(key));
		}finally {
			close(jedis);
		}
	}
	
	
	
	
	private byte[] keyToBytes(String  key) {
		return iSerializer.keyToBytes(key);
	}


	/**
	 * byte数组集合转换成object value集合
	 * @param data
	 */
	public List<Object> bytesListToValueList(List<byte[]> data) {
		List<Object> result = new ArrayList<>();
		for(byte[] bytes : data) {
			result.add(bytesToValue(bytes));
		}
		return result;
	}

	/**
	 * 查找所有符合给定模式 pattern 的 key 。
	 * KEYS * 匹配数据库中所有 key 。
	 * KEYS h?llo 匹配 hello ， hallo 和 hxllo 等。
	 * KEYS h*llo 匹配 hllo 和 heeeeello 等。
	 * KEYS h[ae]llo 匹配 hello 和 hallo ，但不匹配 hillo 。
	 * 特殊符号用 \ 隔开
	 */
	public Set<String> keys(String pattern) {
		Jedis jedis = getJedis();
		try {
			return jedis.keys(pattern);
		} finally {
			close(jedis);
		}
	}

	
	@SuppressWarnings("unchecked")
	public <T> T get(Object key) {
		Jedis jedis = getJedis();
		try {
			return (T) bytesToValue(jedis.get(valueToBytes(key)));
		}finally {
			close(jedis);
		}
	}
	
	protected Object bytesToValue(byte[] bytes) {
		return iSerializer.bytesToObject(bytes);
	}
	
    protected void byteArrayToValues() {
		
	}
	
	public String setex(Object key,Integer seconds, Object value) {
		Jedis jedis = getJedis();
		try {
			return jedis.setex(valueToBytes(key), seconds, valueToBytes(key));
		} finally {
			close(jedis);
		}
	}
	
	/**
	 * set存储
	 * 向名称为key的set中添加元素member
	 * @param key
	 * @param members
	 * @return
	 */
	public Long sadd(Object key,Object... members) {
		Jedis jedis = getJedis();
		try {
			return jedis.sadd(valueToBytes(key), vauleToByteArray(members));
		} finally {
			close(jedis);
		}
	}
	
	public Long srem(Object key,Object... members) {
		Jedis jedis = getJedis();
		try {
			return jedis.srem(valueToBytes(key), vauleToByteArray(members));
		} finally {
			close(jedis);
		}
	}
	
	/**
	 * 随机返回并删除名称为key的set中一个元素
	 * @param key
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T> T spop(Object key) {
		Jedis jedis = getJedis();
		try {
			return (T) bytesToValue(jedis.spop(valueToBytes(key)));
		} finally {
			close(jedis);
		}
	}
	
	/**
	 * 将member元素从名称为srckey的集合移到名称为dstkey的集合
	 * @param srckey
	 * @param dstkey
	 * @param member
	 * @return
	 */
	public Long  smove(Object srckey, Object dstkey, Object member) {
		Jedis jedis = getJedis();
		try {
			 return jedis.smove(valueToBytes(srckey), valueToBytes(dstkey), valueToBytes(member));
		} finally {
			close(jedis);
		}
	}
	
	/**
	 * 返回名称为key的set的基数
	 * @param key
	 */
	public Long scard(Object key) {
		Jedis jedis = getJedis();
		try {
			return jedis.scard(valueToBytes(key));
		} finally {
			close(jedis);
		}
	}
	
	/**
	 * 测试member是否是名称为key的set的元素
	 * @param key
	 * @param member
	 * @return
	 */
	public boolean sismember(Object key , Object member) {
		Jedis jedis = getJedis();
		try {
			return jedis.sismember(valueToBytes(key), valueToBytes(member));
		} finally {
			close(jedis);
		}
	}
	
	/**
	 * 求交集
	 * @param keys
	 */
	@SuppressWarnings("unused")
	public <T> Set<T> sinter(Object... keys) {
		Jedis jedis = getJedis();
		try {
			Set<byte[]> set = jedis.sinter(valueToBytes(keys));
		} finally {
			close(jedis);
		}
		return null;
	}
	
	/**
	 * 求并集
	 * @param keys
	 */
	public void sunion(Object... keys) {
		Jedis jedis = getJedis();
		try {
			jedis.sunion(valueToBytes(keys));
		} finally {
			close(jedis);
		}
	}
	
	/**
	 * 列表存储
	 * @param keys
	 * @param valueArrays
	 * @return
	 */
	public Long lpush(Object key,Object... valueArrays) {
		Jedis jedis = getJedis();
		try {
			return jedis.lpush(valueToBytes(key), vauleToByteArray(valueArrays));
		} finally {
			close(jedis);
		}
	}
	
	
	
	
	
	public Long del(Object... keys) {
		Jedis jedis = getJedis();
		try {
			return jedis.del(vauleToByteArray(keys));
		} finally {
			close(jedis);
		}
	}
	
	
	/**
     * 删除给定的多个 key
     * 不存在的 key 会被忽略。
     */
	protected byte[][] vauleToByteArray(Object... valueArrays) {
		byte[][] data = new byte[valueArrays.length][];
		for (int i=0; i<data.length; i++)
			data[i] = valueToBytes(valueArrays[i]);
		return data;
	}
	
	/**
	 * 使用客户端向 Redis 服务器发送一个 PING ，如果服务器运作正常的话，会返回一个 PONG 。
	 * 通常用于测试与服务器的连接是否仍然生效，或者用于测量延迟值。
	 */
	public String ping() {
		Jedis jedis = getJedis();
		try {
			return jedis.ping();
		}
		finally {close(jedis);}
	}
	
    protected byte[] valueToBytes(Object value) {
		return iSerializer.valueToBytes(value);
	}

    public void setLocalRedis(Jedis jedis) {
    	threadLocal.set(jedis);
    }
    
	public ISerializer getiSerializer() {
		return iSerializer;
	}

	public void setiSerializer(ISerializer iSerializer) {
		this.iSerializer = iSerializer;
	}

	public Jedis getThreadLocal() {
		return threadLocal.get();
	}

	private Jedis getJedis() {
		Jedis jedis = threadLocal.get();
		return jedis == null ? jedisPool.getResource() : jedis;
	}
	
	
	/**
	 * 发布消息
	 */
	public void publish(String channel,String message) {
		Jedis jedis = getJedis();
		jedis.publish(channel, message);
	    close(jedis);
	}
	
	public void subscribe(final JedisPubSub jedisPubSub, final String... channels) {
		Jedis jedis = getJedis();
		try {
			new Thread(new Runnable() {
				@Override
				public void run() {
					jedis.subscribe(jedisPubSub, channels);
					logger.info("频道订阅成功.......");
				}
			}).start();
		} catch (Exception e) {
			logger.info("频道订阅异常.......",e);
		} finally {
			close(jedis);
		}
	}
	
	public void subscribe(final JedisPubSub jedisPubSub) {
		Jedis jedis = getJedis();
		try {
			new Thread(new Runnable() {
				@Override
				public void run() {
					jedis.subscribe(jedisPubSub, jfastRedisConfig.getChannel());
					logger.info("频道订阅成功.......");
				}
			}).start();
		} catch (Exception e) {
			logger.info("频道订阅异常.......",e);
		} finally {
			close(jedis);
		}
	}
	
	public void close(Jedis jedis) {
		if (threadLocal.get() == null && jedis != null)
			jedis.close();
	}
}
