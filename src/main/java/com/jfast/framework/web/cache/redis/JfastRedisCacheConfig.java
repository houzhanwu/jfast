/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.web.cache.redis;

import com.jfast.framework.web.annotation.ConfigurationProperties;

/**
 * redis 缓存配置
 * @author zengjintao
 * @version 1.0
 * @createTime 2017年12月24日下午6:29:06
 */
@ConfigurationProperties(prefix = "jfast.redis.cache")
public class JfastRedisCacheConfig extends JfastRedisConfig {

}
