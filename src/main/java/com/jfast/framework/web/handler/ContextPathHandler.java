/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.web.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.jfast.framework.kit.StrKit;

public class ContextPathHandler extends Handler {

	private String contextPath;
	
	public ContextPathHandler(){
		this.contextPath = "BASE_PATH";
	}
	
	public String getContextPath() {
		return contextPath;
	}

	public void setContextPath(String contextPath) {
		this.contextPath = contextPath;
	}

	public ContextPathHandler(String contextPath){
		this.contextPath = contextPath;
	}
	
	@Override
	public void doHandler(String target, HttpServletRequest request, HttpServletResponse response) {
		if (StrKit.isEmpty(contextPath)) {
			throw new NullPointerException("contextPath can not be null");
		}
		request.setAttribute(contextPath, request.getContextPath());
		nextHandler.doHandler(target, request, response);
	}
}
