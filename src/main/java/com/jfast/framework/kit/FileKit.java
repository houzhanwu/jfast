/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.kit;

import java.io.File;

public class FileKit {
	
	/**
	 * 删除文件
	 * @param file
	 */
	public static void delete(File file) {
		if (file != null && file.exists()) {
			if (file.isFile()) {
				file.delete();
			}
			else if (file.isDirectory()) {
				File files[] = file.listFiles();
				if (files != null) {
					for (int i=0; i<files.length; i++) {
						delete(files[i]);
					}
				}
				file.delete();
			}
		}
	}
	
	public static String getFileExtension(String fileFullName) {
	    if (StrKit.isEmpty(fileFullName)) {
            throw new RuntimeException("fileFullName is empty");
        }
	    return  getFileExtension(new File(fileFullName));
	}
	
	public static String getFileExtension(File file) {
	    if (file == null) {
	        throw new NullPointerException();
	    }
	    String fileName = file.getName();
	    int dotIdx = fileName.lastIndexOf('.');
	    return (dotIdx == -1) ? "" : fileName.substring(dotIdx + 1);
    }
}
