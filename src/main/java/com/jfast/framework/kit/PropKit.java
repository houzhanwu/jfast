/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.kit;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;


public class PropKit {
	
	private  Properties properties = new Properties();
	
	private String fileName;
	
	public PropKit() {
		
	}
	
	public void use(String fileName) {
		this.fileName = fileName;
		loadProperties();
	}
	
	public PropKit(Properties properties) {
		this.properties = properties;
	}
	
	public PropKit(String fileName) {
		this.fileName = fileName;
		loadProperties();
	}
	
	private PropKit loadProperties() {
		InputStream in = null;
		try {
			in = Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName);
			properties.load(new InputStreamReader(in, "UTF-8"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}

	public PropKit(File file, String encoding) {
		loadFileProperties(file,encoding);
	}
	
	private PropKit loadFileProperties(File file, String encoding) {
		 getProperties(file,encoding);
		 return this;
	}

	private Properties getProperties(File file, String encoding) {
		InputStream in = null;
		try {
			in = new FileInputStream(file);
			properties.load(new InputStreamReader(in, encoding));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return properties;
	}

	@SuppressWarnings("unchecked")
	private Enumeration<String> getAllKey() {
		return (Enumeration<String>) properties.propertyNames();
	}
	
	public List<String> getKeyByValue(String value) {
		List<String> propKeys = new ArrayList<>();
		Enumeration<String> enumeration = getAllKey();
		while (enumeration.hasMoreElements()) {
			String key = enumeration.nextElement();
			if (key.startsWith(value)) {
				propKeys.add(key);
			}
		}
		return propKeys;
	}
	
	public List<String> getKeys() {
		List<String> propKeys = new ArrayList<>();
		Enumeration<String> enumeration = getAllKey();
		while (enumeration.hasMoreElements()) {
			String key = enumeration.nextElement();
			propKeys.add(key);
		}
		return propKeys;
	}
	
	public Map<String, Object> getKeyAndValue() {
		Map<String, Object> properties = new HashMap<>();
		List<String> keys = getKeys();
		for (String key : keys) {
			properties.put(key,get(key));
		}
		return properties;
	}
	
	public String get(String key){
		return properties.getProperty(key);
	}
	
	public String get(String key,String defaultValue) {
		if (get(key) != null) {
			return get(key);
		}
		return defaultValue;
	}
	
	public Integer getInt(String key) {
		return getInt(key,null);
	}
	
	public Integer getInt(String key,Integer defaultValue) {
		String value= get(key);
		if(key != null){
			return Integer.parseInt(value.trim());
		}
		return defaultValue;
	}

	public long getLong(String key) {
		return getLong(key,null);
	}
	
	
	public long getLong(String key,Long defaultValue) {
		String value = get(key);
		if (key != null) {
			return Long.parseLong(value.trim());
		}
		return defaultValue;
	}
	
	public Boolean getBoolean(String key) {
		return getBoolean(key,null);
	}
	
	public Boolean getBoolean(String key,Boolean defaultValue) {
		String value = get(key);
		if (value != null) {
			value = value.toLowerCase();
			if (value.equals("true")) {
				return true;
			} else if(value.equals("false")) {
				return false;
			}
			throw new RuntimeException("The value can not parse to Boolean : " + value);
		}
		return defaultValue;
	}
	
	public Properties getProperties() {
		return properties;
	}
}
