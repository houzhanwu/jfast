/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.kit;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gson.Gson;

public class JsonKit {
	
	private static final Gson gson = new Gson();
	
	public static String toJson(Object object) {
		return gson.toJson(object);
	}
	
	/**
	 * 将map转换为json
	 * @param map
	 * @return
	 */
	public static String toJson(Map<String, String> map) {
		if (map == null) {
			throw new NullPointerException("map can not be null");
		}
		Set<String> keySet = map.keySet();
		String lastKey = getSetLastKey(keySet.iterator());
		StringBuilder sb = new StringBuilder("{");
		for (String key : keySet) {
			sb.append("\"");
			sb.append(key + "\":").append("\"" + map.get(key) + "\"");
			if (key == lastKey) {
				sb.append("");
			} else {
				sb.append(",");
			}
		}
		sb.append("}");
		return sb.toString();
	}
	
	/**
	 * 获取set集合中的最后一个key
	 * @param iterator
	 * @return
	 */
	public static String getSetLastKey(Iterator<String> iterator) {
		List<String> list = new ArrayList<String>();
		while (iterator.hasNext()) {
			list.add(iterator.next());
		}
		return list.get(list.size()-1);
	}
}
