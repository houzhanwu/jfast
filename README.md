# jfast
 它是一款轻量级 Java Web 框架,基于MVC架构

- 内置 IOC(基于guice)、AOP(基于guice,cglib)、ORM、DAO、MVC 等特性
- 基于 Servlet 3.0 规范
- 使用 Java 注解取代 XML 配置
- 支持jsp,velocity等视图
- 面向基于 Web 的中小规模的应用程序
- 新手能在较短时间内入门
- 核心具有良好的定制性且插件易于扩展

# 入门教程

1. 创建一个 Maven Web 工程
整个工程的目录结构如下：

```
Jfast-demo/
　　┗ src/
　　　　┗ main/
　　　　　　┗ java/
　　　　　　┗ resources/
　　　　　　┗ webapp/
　　┗ pom.xml
在 java 目录下，创建以下包名目录结构：

org/
　　┗ jfast/
　　　　┗ demo/
　　　　　　┗ contrller/
　　　　　　┗ entity/
　　　　　　┗ service/
```

 >**可见，基础包名为：com.jfast，下面的配置中会用到它。** 

2. 配置 Maven 依赖
编辑 pom.xml 文件，添加 smart-framework 依赖：


```
 **<dependency>
    <groupId>com.jfast</groupId>
	<artifactId>jfast</artifactId>
    <version>1.0</version>
</dependency>** 
```


3.编写Jfast核心配置文件

在 `resources` 目录下，创建一个名为 `jfast.properties` 的文件，内容如下：

##jfast基础配置
```
##配置包扫描路径
jfast.basePackage=com.jfast
##配置视图类型
jfast.viewType=jsp
##配置view资源路径
jfast.baseViewPath=/WEB-INF/jsp/

##数据源配置

jfast.dataSource.type=mysql

jfast.dataSource.url=jdbc:mysql://localhost:3306/jfast?characterEncoding=utf8

jfast.dataSource.username=root

jfast.dataSource.password=123456
```

读取properties文件属性值
在jfast中，只需使用@ConfigurationProperties注解即可

配置文件

要读取jfast.properties的配置信息非常简单，例如我们配置内容如下：

```

jfast.myconfig.name=aaa
jfast.myconfig.passowrd=bbb
jfast.myconfig.age=10
```

要读取这个配置信息，我们需要定义我们的一个model类，并通过@ConfigurationProperties注解给我们的类配置上类与配置文件的对应关系，如下所示：


```
@ConfigurationProperties(prefix="jfast.myconfig")
public class MyConfigModel{
    private String name;
    private String password;
    private int age;

    //getter setter 略
}
```
//更高级用法
@ConfigurationProperties可以指定要读取的properties文件


> 提示：需根据实际情况修改以上配置。

4.开始代码开发
  
```
/**
数据库实体类
**/
@Table(name ="user_info",primaryKey = {"id"})
public class UserInfo {

    private long id;

    private String username;

    private String password;

    // getter/setter
}

```

  
```
/**
编写Controller
**/
@Controller
@RequestMapping("/user")
public class UserController {

    @Inject
    private UserService userService;
    
    
    public void index(){
        
    }

    @RequestMapping("/list")
    public void list(){
        
    }
    
    //提示:方法中的@RequestMapping可以进行省略，此时jfast将会把方法名当成
           actionKey进行url映射
}

```

```
/**
编写UserService 接口
**/
@Controller
@RequestMapping("/user")
public interface UserService {

   //定义接口方法
}

```

```
/**
编写UserService 接口
**/
@Bean
public interface UserServiceImpl implements  UserService{

   
}

```

5.ORM层的使用
 

```
  5.1 DefaultJdbcTemplate
   
  //返回实体类对象
   UserInfo userInfo = jdbcTemplate.findOne("select * from user_info where id = ?",UserInfo.class,1);
		return userInfo;

  //返回实体类集合
   List<UserInfo> list= jdbcTemplate.findAll("select * from user_info where id = ?",UserInfo.class,1);
		return userInfo;
  //返回map集合
   Map<String, Object> reMap = jdbcTemplate.findMap("select id,password from user_info where id = ?","information",1);
		return rep;
 //insert操作
 UserInfo userInfo = new UserInfo();
 userInfo.setName("hello");
 userInfo.setPassword("123456");
 boolean flag = jdbcTemplate.insert(userInfo);

```
5.2 数据库事物

 在jfast中对于数据库事物的处理，您无需编写任何代码，只需使用@Transaction注解即可完成，代码入下
 
 
 
```
@Bean
 public class JfastServiceImpl implements JfastService{

	@Transaction
	public void login(){
		
	}
 }
```

3.多源数据库配置

```
jfast.dataSource.information.url=jdbc:mysql://localhost:3306/information?characterEncoding=utf8
jfast.dataSource.master.username=root
jfast.dataSource.master.password=123456
jfast.dataSource.master.basePackage=com.jfast.entity.master
jfast.dataSource.master.dataSourceClassName=com.jfast.framework.web.orm.dataSource.DefaultDataSourceFactory


jfast.dataSource.slave.url=jdbc:mysql://localhost:3306/stroe?characterEncoding=utf8
jfast.dataSource.slave.username=root
jfast.dataSource.slave.password=123456
jfast.dataSource.slave.basePackage=com.jfast.entity.slave
jfast.dataSource.slave.dataSourceClassName=com.jfast.framework.web.orm.dataSource.DefaultDataSourceFactory
```

多源数据库事物

```
@Bean
 public class JfastServiceImpl implements JfastService{

	@Transaction（configName = "slave"）
	public void login(){
		
	}

        @Transaction（configName = "master"）
	public void insert(){
		
	}
 }
```